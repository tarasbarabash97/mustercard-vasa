import Axios from "axios";
import { load } from "cheerio";
import { FiatService } from "./FiatService";

export class MeestTransfer extends FiatService {
    __tableHeader = [
        "Receiver",
        "To Send",
        null,
        "Rate",
        "To Receive",
        null,
        "Rate",
        "Result",
        null,
        "Fee",
        null,
        "Profit (no fee), %",
        "Profit (+fee), %"
    ]

    static __instance;

    __axios = Axios.create({
        baseURL: "https://meestpay.com/",
        headers: {
            "User-Agent": " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
        },
        validateStatus: () => true
    });

    static getInstance() {
        if (!this.__instance) this.__instance = new MeestTransfer();
        return this.__instance;
    }

    constructor() {
        super();

        this.__axios.interceptors.request.use((config) => {
            if (process.env.DEMO_REQUEST)
                console.log(JSON.stringify(config, null, "\t"));
            return config;
        })

        this.__axios.interceptors.response.use(async (response) => {
            if (process.env.DEMO_REQUEST && !/html/.test(response.headers["content-type"]))
                console.log(response.status, JSON.stringify(response.data, null, "\t"));
            return response;
        });
    }

    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];

        const { data } = await this.__axios("/");
        const $ = load(data);

        let allCurrencies = $("#payout_currency > option").toArray().map(i => $(i).text().trim().split(" ").pop()).filter(i => i);

        allCurrencies = allCurrencies.sort((a, b) => a.localeCompare(b));

        if (process.env.DEMO_REQUEST)
            allCurrencies = allCurrencies.slice(0, 5);

        for (let i of allCurrencies) {
            const currency = i;

            if (i === this.__baseCurrency) continue;

            if (process.env.DEMO_REQUEST)
                console.log(this.constructor.name, `${this.__baseCurrency} -> ${currency} -> ${this.__baseCurrency}`);

            const firstColumns = [];

            try {
                const requestBody = {
                    payment_currency: this.__baseCurrency,
                    payout_currency: currency,
                    total_amount: this.__baseAmount
                };

                const { data } = await this.__axios.post(
                    "/wp-content/themes/meest/calc_fun.php",
                    new URLSearchParams(requestBody),
                    { headers: { "Content-Type": "application/x-www-form-urlencoded" } }
                );

                const { charges, payout_amount, rate_info } = data;

                const amountToReceive = parseFloat(payout_amount.replace(/\s+/g, "").replace(",", "."));
                const currencyRate = parseFloat(rate_info.split("=").pop().trim().replace(/\s+/g, "").replace(",", "."));
                const fee = parseFloat(charges.replace(/\s+/g, "").replace(",", "."));

                const result = await this.__obtainAllExchangeRates({ sellAmount: amountToReceive, fromCurrency: currency, toCurrency: this.__baseCurrency });

                lines.push(...result.map(i => {
                    const { name, result } = i;
                    if (result) {
                        const { result: { rate, amount } } = i;
                        if (isFinite(rate))
                            return [
                                ...firstColumns,
                                name,
                                this.__baseAmount,
                                this.__baseCurrency,
                                currencyRate,
                                amountToReceive,
                                currency,
                                rate,
                                amount,
                                this.__baseCurrency,
                                fee,
                                this.__baseCurrency,
                                (amount - this.__baseAmount) / this.__baseAmount,
                                (amount - (this.__baseAmount + fee)) / this.__baseAmount,
                            ];
                    }
                }));

                await new Promise(resolve => setTimeout(resolve, 2500));
            } catch (err) {
                console.log("Failed to obtain", this.constructor.name, "info for", currency, ":", err.message)
            }
        }
        return lines;
    }
}