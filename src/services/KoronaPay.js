import Axios from "axios";
import { FiatService } from "./FiatService";

export class KoronaPay extends FiatService {
    __tableHeader = [
        "Receiver",
        "To Send",
        null,
        "Rate",
        "To Receive",
        null,
        "Rate",
        "Result",
        null,
        "Profit (no fee), %"
    ]

    static __instance;

    __baseSenderCountry = "DEU";

    __axios = Axios.create({
        baseURL: "https://api.koronapay.com/transfers",
        headers: {
            "accept": "application/vnd.cft-data.v2.89+json",
            "accept-language": "en",
            "user-agent": "okhttp/4.8.0"
        },
        validateStatus: () => true
    });

    static getInstance() {
        if (!this.__instance) this.__instance = new KoronaPay();
        return this.__instance;
    }

    constructor() {
        super();

        this.__axios.interceptors.request.use((config) => {
            if (process.env.DEMO_REQUEST)
                console.log(JSON.stringify(config, null, "\t"));
            return config;
        })

        this.__axios.interceptors.response.use(async (response) => {
            if (process.env.DEMO_REQUEST && !/html/.test(response.headers["content-type"]))
                console.log(response.status, JSON.stringify(response.data, null, "\t"));
            return response;
        });

        this.__destinations = this.__axios(`/directions/points/receiving?sendingCountryId=${this.__baseSenderCountry}`).catch((err) => ({ data }));
    }

    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];

        let { data: destinations } = await this.__destinations;

        const testedCurrencies = [];

        if (process.env.DEMO_REQUEST)
            destinations = destinations.slice(0, 2);

        for (let i of destinations.filter(i => !i.forbidden).sort((a, b) => a.country.name.localeCompare(b.country.name))) {
            try {
                const { country: { id: country } } = i;

                const infoRequestParams = new URLSearchParams({
                    "sendingCountryId": this.__baseSenderCountry,
                    "receivingCountryId": country,
                    "forTransferRepeat": false
                })

                const { data: info } = await this.__axios(`/tariffs/info?${infoRequestParams.toString()}`);

                for (let method of info.filter(
                    i => i.sendingCurrency.code === this.__baseCurrency
                        && /card/i.test(i.paymentMethod)
                        && /card/i.test(i.receivingMethod)
                )) {
                    let amountToSend = this.__baseAmount;
                    try {
                        const { sendingCurrency: { id }, receivingCurrency: { id: receivingId, code: currency }, paymentMethod, receivingMethod, minSendingAmount } = method;

                        if (currency !== this.__baseCurrency && !testedCurrencies.includes(currency)) {
                            amountToSend = Math.max(amountToSend, minSendingAmount);
                            amountToSend += 100;

                            if (process.env.DEMO_REQUEST)
                                console.log(this.constructor.name, `${this.__baseCurrency} -> ${currency} -> ${this.__baseCurrency} (${paymentMethod}, ${receivingMethod})`);

                            const requestParams = new URLSearchParams({
                                "sendingCountryId": this.__baseSenderCountry,
                                "sendingCurrencyId": id,
                                "receivingCountryId": country,
                                "receivingCurrencyId": receivingId,
                                "sendingAmount": amountToSend,
                                "receivingMethod": receivingMethod,
                                "paidNotificationEnabled": "false",
                                "paymentMethod": paymentMethod,
                            });

                            const { data } = await this.__axios(`https://api.koronapay.com/transfers/tariffs?${requestParams.toString()}`);

                            const [{ receivingAmount: amountToReceive, exchangeRate }] = data;

                            const result = await this.__obtainAllExchangeRates({ sellAmount: amountToReceive, fromCurrency: currency, toCurrency: this.__baseCurrency });

                            lines.push(...result.map(i => {
                                const { name, result } = i;
                                if (result) {
                                    const { result: { rate, amount } } = i;
                                    if (isFinite(rate))
                                        return [
                                            name,
                                            amountToSend,
                                            this.__baseCurrency,
                                            exchangeRate,
                                            amountToReceive,
                                            currency,
                                            rate,
                                            amount,
                                            this.__baseCurrency,
                                            (amount - amountToSend) / amountToSend
                                        ];
                                }
                            }));

                            testedCurrencies.push(currency);
                        }

                        await new Promise(resolve => setTimeout(resolve, 2500));
                    } catch (err) {
                        console.log("Failed to obtain", this.constructor.name, "info for transfer with method", JSON.stringify(method), ", error:", err.message);
                    }
                }
            } catch (err) {
                console.log("Failed to obtain", this.constructor.name, "info:", err.message)
            }
        }
        return lines;
    }
}