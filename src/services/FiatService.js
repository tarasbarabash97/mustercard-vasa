import { AutoQueue } from "./Queue";
import axios from "axios";
import moment from "moment";
import allCurrencies from "../../static/mastercard_currencies.json";

export const ExchangeServices = {
    "MASTERCARD": "Mastercard",
    "VISA": "Visa",
    "WISE": "Wise",
    "REVOLUT": "Revolut"
}

export const DefaultAmounts = {
    "EUR": 250,
    "GBP": 250,
    "PLN": 1000,
    "UAH": 10000,
    "CZK": 6000
}

export class FiatService {
    __baseCurrency = "EUR";
    __baseAmount = 250;

    __exchangeServicesFuncs = [
        { name: ExchangeServices.MASTERCARD, func: this.__obtainMastercardRate, isPaymentSystem: true },
        { name: ExchangeServices.VISA, func: this.__obtainVisaRate, isPaymentSystem: true },
        { name: ExchangeServices.WISE, func: this.__obtainWiseInfo },
        { name: ExchangeServices.REVOLUT, func: this.__obtainRevolutInfo },
    ];
    __paymentSystems = this.__exchangeServicesFuncs.filter(i => i.isPaymentSystem).map(i => i.name);

    __queue = new AutoQueue();
    __processQueue = new AutoQueue();
    static __instance;

    static getInstance() {
        if (!this.__instance) this.__instance = new FiatService();
        return this.__instance;
    }

    _obtainInfo = async (baseCurrency = "EUR") => {
        if (!allCurrencies.includes(baseCurrency)) return { [baseCurrency]: [] }

        this.__baseCurrency = baseCurrency;

        if (DefaultAmounts[this.__baseCurrency]) this.__baseAmount = DefaultAmounts[this.__baseCurrency];
        else this.__baseAmount = DefaultAmounts.EUR;

        console.log(this.constructor.name, this.__baseAmount, this.__baseCurrency, "started");

        const result = await this.__processQueue.enqueue(() => this.__obtainInfo(baseCurrency));

        console.log(this.constructor.name, this.__baseAmount, this.__baseCurrency, "finished");

        return { [baseCurrency]: (result || []).filter(i => i) };
    }

    __obtainAllExchangeRates({ sellAmount, buyAmount, fromCurrency, toCurrency }) {
        const promise = () => Promise.allSettled(this.__exchangeServicesFuncs.map(async i => {
            return new Promise(resolve => setTimeout(async () => resolve({
                name: i.name,
                result: await this.__obtainExchangeRate(i.func, { sellAmount, buyAmount, fromCurrency, toCurrency })
            })), 1500);
        }));
        return this.__queue.enqueue(async () => {
            const result = await promise();
            return result.map(i => i.value);
        });
    }

    async __obtainExchangeRate(func, { sellAmount, buyAmount, fromCurrency, toCurrency }) {
        let result;
        try {
            if (isFinite(buyAmount)) {
                result = await func(buyAmount, toCurrency, fromCurrency);
                result = { rate: result, amount: buyAmount * result };
            }
            if (isFinite(sellAmount)) {
                result = await func(sellAmount, fromCurrency, toCurrency);
                result = { rate: result, amount: sellAmount / result };
            }
            if (result.rate < 1) result.rate = 1 / result.rate;
        } catch (err) {
            console.log(func.name, "got an error with arguments", JSON.stringify(arguments), err.message);
        }
        return result;
    }

    async __obtainVisaRate(amountToCharge, baseCurrency, currency) {
        const { data: { fxRateWithAdditionalFee } } = await axios(`https://usa.visa.com/cmsapi/fx/rates?amount=${amountToCharge}&fee=0&utcConvertedDate=${moment().format("MM/DD/YYYY")}&exchangedate=${moment().format("MM/DD/YYYY")}&fromCurr=${baseCurrency}&toCurr=${currency}`);
        const rate = parseFloat(fxRateWithAdditionalFee.replace(",", ""));
        return rate;
    }

    async __obtainMastercardRate(amountToCharge, baseCurrency, currency) {
        const { data: { data } } = await axios(`https://www.mastercard.us/settlement/currencyrate/conversion-rate?fxDate=0000-00-00&transCurr=${currency}&crdhldBillCurr=${baseCurrency}&bankFee=0&transAmt=${amountToCharge}`);
        const { conversionRate } = data;
        return conversionRate;
    }

    async __obtainWiseInfo(amountToCharge, baseCurrency, currency) {
        const { data } = await axios(`https://wise.com/gateway/v3/price?sourceAmount=${amountToCharge}&sourceCurrency=${baseCurrency}&targetCurrency=${currency}&priceSetId=319&markers=FCF_PRICING`);
        const result = data.find(i => i.payOutMethod == "BALANCE" && i.payInMethod == "BALANCE");
        return result.sourceAmount / result.targetAmount;
    }

    async __obtainRevolutInfo(amountToCharge, baseCurrency, currency) {
        let fraction;
        try {
            const { fraction: fr } = currenciesData[currency];
            fraction = fr;
        } catch (err) {
            fraction = 2;
        }
        const { data: revolutData, headers } = await axios(`https://www.revolut.com/api/exchange/quote?amount=${Math.round(amountToCharge * Math.pow(10, fraction))}&country=PL&fromCurrency=${baseCurrency}&isRecipientAmount=false&toCurrency=${currency}`, {
            headers: {
                "accept-language": "en-US;q=1",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
            }
        });
        if (!/json/gi.test(headers["content-type"])) return;

        let { recipient: { amount }, rate: { rate } } = revolutData;
        amount = amountToCharge * rate;
        try {
            const feeAmount = revolutData.plans[0].fees.total.amount;
            amount = amount - feeAmount * rate / Math.pow(10, fraction);
        } catch (err) { console.log("fee calc fail", err.message) };
        return amountToCharge / amount;
    }
}