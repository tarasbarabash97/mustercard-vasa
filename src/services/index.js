export * from "./FiatService";

export * from "./TransferGo";
export * from "./Profee";
export * from "./RebellPay";
export * from "./Findo";
export * from "./MeestTransfer";
export * from "./MultiSystem";
export * from "./KoronaPay";

import * as services from ".";
import { AutoQueue } from "./Queue";

export function getService(name) {
    return SingletonWrapper.getInstance(services[name].getInstance());
}

class SingletonWrapper {
    __processQueue = new AutoQueue();

    static __instance;

    constructor(child) {
        this.__child = child;
    }

    static getInstance(child) {
        if (!this.__instance)
            this.__instance = new SingletonWrapper(child);
        this.__instance.__child = child;
        return this.__instance;
    }

    obtainInfo(baseCurrency) {
        const child = this.__child;
        const process = this.__processQueue.enqueue(() => child._obtainInfo(baseCurrency));
        return process;
    }
}