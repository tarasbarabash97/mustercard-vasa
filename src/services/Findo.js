import Axios from "axios";
import findoCurrencies from "../../static/findo_currencies.json";
import { ExchangeServices, FiatService } from "./FiatService";

export class Findo extends FiatService {
    __tableHeader = [
        "Sender System",
        "Receiver System",
        "Receiver",
        "To Send",
        null,
        "Rate",
        "To Receive",
        null,
        "Rate",
        "Result",
        null,
        "Profit, %"
    ]

    static __instance;

    __axios = Axios.create({
        baseURL: "https://api.fin.do/v1/api/fin/AssumeCommission",
        headers: {
            "user-agent": "okhttp/4.8.0"
        },
        validateStatus: () => true
    });

    static getInstance() {
        if (!this.__instance) this.__instance = new Findo();
        return this.__instance;
    }

    constructor() {
        super();
        this.currencies = this.__axios("/info/currency/", { baseURL: "https://api.rebellpay.com" }).catch(err => ({ data: { data: { allCurrencies } } }));

        this.__axios.interceptors.request.use((config) => {
            // if (process.env.DEMO_REQUEST)
            //     console.log(JSON.stringify(config, null, "\t"));
            return config;
        })

        this.__axios.interceptors.response.use(async (response) => {
            // if (process.env.DEMO_REQUEST)
            //     console.log(response.status, JSON.stringify(response.data, null, "\t"));
            return response;
        });
    }
    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];

        let { data: { data: { allCurrencies } } } = await this.currencies;
        allCurrencies = Object.keys(allCurrencies).sort((a, b) => a.localeCompare(b));

        if (process.env.DEMO_REQUEST)
            allCurrencies = allCurrencies.slice(0, 1);

        for (let i of allCurrencies) {
            const currency = i;

            if (i === this.__baseCurrency) continue;

            // if (process.env.DEMO_REQUEST)
            console.log(this.constructor.name, `${this.__baseCurrency} -> ${currency} -> ${this.__baseCurrency}`);

            const firstColumns = [];

            const formatPaymentSystems = this.__paymentSystems.map(i => i.toUpperCase());

            for (let senderPaymentSystem of formatPaymentSystems)
                for (let receiverPaymentSystem of formatPaymentSystems)
                    try {
                        const requestBody = {
                            "amount": this.__baseAmount.toString(),
                            "type": "SENDER",
                            "receiver": {
                                "sourceType": "COUNTRY",
                                "currency": currency,
                                "country": "PL",
                            },
                            "sender": {
                                "sourceType": "COUNTRY",
                                "currency": this.__baseCurrency,
                                "country": "PL"
                            },
                            "provider": senderPaymentSystem,
                            "providerForReceiver": receiverPaymentSystem
                        };

                        const { data } = await this.__axios.post("/", requestBody);

                        if (!data || !data.result) continue;

                        const { payload: { receiver: { amountToReceive }, rate: { currencyRate } } } = data;

                        let paymentSystems = [];

                        if (new RegExp(ExchangeServices.MASTERCARD, "i").test(receiverPaymentSystem))
                            paymentSystems = this.__exchangeServicesFuncs.filter(i => i.name !== ExchangeServices.VISA && i.name !== ExchangeServices.WISE)

                        if (new RegExp(ExchangeServices.VISA, "i").test(receiverPaymentSystem))
                            paymentSystems = this.__exchangeServicesFuncs.filter(i => i.name !== ExchangeServices.MASTERCARD);

                        const result = await Promise.allSettled(
                            paymentSystems.map(
                                async i => ({
                                    result: await this.__obtainExchangeRate(i.func, { sellAmount: amountToReceive, fromCurrency: currency, toCurrency: this.__baseCurrency }),
                                    name: i.name
                                })
                            )
                        );

                        lines.push(...result.map(i => {
                            const { name, result } = i.value;
                            if (result) {
                                const { result: { rate, amount } } = i.value;
                                if (isFinite(rate))
                                    return [
                                        ...firstColumns,
                                        senderPaymentSystem,
                                        receiverPaymentSystem,
                                        name,
                                        this.__baseAmount,
                                        this.__baseCurrency,
                                        currencyRate,
                                        amountToReceive,
                                        currency,
                                        rate,
                                        amount,
                                        this.__baseCurrency,
                                        (amount - this.__baseAmount) / this.__baseAmount,
                                    ];
                            }
                        }));

                        await new Promise(resolve => setTimeout(resolve, 2500));
                    } catch (err) {
                        console.log("Failed to obtain", this.constructor.name, "info for", currency, ":", err.message)
                    }
        }
        return lines;
    }
}