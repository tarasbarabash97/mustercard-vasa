import { FiatService } from ".";
import Axios from "axios";

export class Profee extends FiatService {
    __axios = Axios.create({
        baseURL: "https://terminal.profee.com/api/v2/terminal"
    });
    __tableHeader = [
        "Receiver",
        "To Send",
        null,
        "Rate",
        "To Receive",
        null,
        "Rate",
        "Result",
        null,
        "Profit, %"
    ];

    static __instance;

    static getInstance() {
        if (!this.__instance) this.__instance = new Profee();
        return this.__instance;
    }

    constructor() {
        super();
        this.__restrictonsRequest = this.__axios("/widgets/100001/settings").catch((err) => {
            console.log(this.constructor.name, "Failed to obtain required info: ", err.message);
            this.__restrictonsRequest = ({ data: { body: { paymentMethodsRestrictions: [] } } });
            return this.__restrictonsRequest;
        })
        this.__countriesRequest = this.__axios("/transfer-countries", { baseURL: "https://www.profee.com/api" }).catch((err) => {
            console.log(this.constructor.name, "Failed to obtain required info: ", err.message);
            this.__countriesRequest = ({ data: { body: [] } });
            return this.__countriesRequest;
        });
    }

    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];

        const { data: { body: { paymentMethodsRestrictions } } } = await this.__restrictonsRequest;
        const { data: { body } } = await this.__countriesRequest;

        let destinations = paymentMethodsRestrictions.filter(i => Object.values(i.availableCurrencyPairs).map(i => i.from.currency).includes(this.__baseCurrency));

        if (process.env.DEMO_REQUEST) destinations = destinations.slice(0, 5);

        for (let i of destinations) {
            const { paymentMethodId, availableCurrencyPairs } = i;
            const [{ from: { currency: from }, to: { currency } }] = Object.values(availableCurrencyPairs);
            const destinationCountry = body.find(i => i.currencies.some(i => i.code === currency));

            if (this.__baseCurrency === currency) continue;

            if (process.env.DEMO_REQUEST)
                console.log(this.constructor.name, `${this.__baseCurrency} -> ${currency} -> ${this.__baseCurrency}`);

            const firstColumns = [];

            try {
                let { data } = await this.__axios.post("/precalcs", {
                    "methodId": paymentMethodId,
                    "methodType": "MT_C2C",
                    "from": { "currency": this.__baseCurrency, "amount": this.__baseAmount, "country": 724 },
                    "to": { "currency": currency, "amount": null, "country": destinationCountry.code }
                });

                const { body: { currencyRate: { rate: currencyRate }, toSum: { amount: amountToReceive } } } = data;

                const result = await this.__obtainAllExchangeRates({ sellAmount: amountToReceive, fromCurrency: currency, toCurrency: this.__baseCurrency });

                lines.push(...result.map(i => {
                    const { name, result } = i;
                    if (result) {
                        const { result: { rate, amount } } = i;
                        if (isFinite(rate))
                            return [
                                ...firstColumns,
                                name,
                                this.__baseAmount,
                                this.__baseCurrency,
                                currencyRate,
                                amountToReceive,
                                currency,
                                rate,
                                amount,
                                this.__baseCurrency,
                                (amount - this.__baseAmount) / this.__baseAmount,
                            ];
                    }
                }));

                await new Promise(resolve => setTimeout(resolve, 2500));
            } catch (err) { console.log("Failed to obtain", this.constructor.name, "info for", currency, ":", err.message) }
        }

        return lines;
    }
}