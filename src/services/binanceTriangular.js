import Axios from "axios";
import { AutoQueue } from "./Queue";
import { workerData, isMainThread, parentPort, Worker } from "worker_threads";

const OrderTypes = {
    "LIMIT": "limit",
    "MARKET": "market"
}

class BinanceTriangle {
    static __instance;
    __iterationPaths = {};
    __flag = false;
    __queue = new AutoQueue();

    static getInstance() {
        if (this.__instance == undefined)
            this.__instance = new BinanceTriangle();
        return this.__instance;
    }

    async __obtainBinanceInfo() {
        const { data: info } = await Axios("https://api.binance.com/api/v3/exchangeInfo");
        let { data } = await Axios("https://api.binance.com/api/v3/ticker/bookTicker");
        const { data: dailyStat } = await Axios("https://api.binance.com/api/v3/ticker/24hr");
        data = data.map(i => {
            const { symbol } = i;
            const pairInfo = info.symbols.find(i => i.symbol === symbol);
            const pairStat = dailyStat.find(i => i.symbol === symbol);
            if (pairInfo && pairStat && pairInfo.status === "TRADING" && pairInfo.isSpotTradingAllowed)
                return {
                    ...i,
                    baseAsset: pairInfo.baseAsset,
                    quoteAsset: pairInfo.quoteAsset,
                    orderCount: pairStat.count
                }
            return;
        }).filter(i => i);

        this.__data = data;
        this.__avgOrderCount = Math.round(this.__data.reduce((a, b) => a + b.orderCount, 0) / this.__data.length);
        return this.__data;
    }

    __buildMatrix(isMarket = false) {
        const symbols = this.__data.reduce((a, b) => {
            if (!a.includes(b.baseAsset)) a.push(b.baseAsset);
            if (!a.includes(b.quoteAsset)) a.push(b.quoteAsset);

            return a;
        }, []).sort((a, b) => a.localeCompare(b));

        const matrix = Array.from(Array(symbols.length), () => new Array(symbols.length));

        for (let [baseIndex, base] of Object.entries(symbols))
            for (let [quoteIndex, quote] of Object.entries(symbols)) {
                let cost = 0;
                const pair = this.__data.find(i =>
                    [base, quote].includes(i.baseAsset) && [base, quote].includes(i.quoteAsset)
                );
                if (pair) {
                    const { askPrice, bidPrice, baseAsset, quoteAsset } = pair;
                    if (base == baseAsset && quote === quoteAsset) cost = isMarket ? parseFloat(bidPrice) : parseFloat(askPrice);
                    else cost = isMarket ? parseFloat(askPrice) : parseFloat(bidPrice);
                }
                matrix[baseIndex][quoteIndex] = isFinite(cost) ? cost : 0;
            }
        this.__symbols = symbols;
        this.__matrix = matrix;
        return { symbols, matrix };
    }

    __findPaths(node, curPath = [], price = []) {
        const value = this.__symbols.findIndex(i => i === node);
        const start = curPath[0];

        curPath.push(node);

        if (curPath.length > this.__maxDepth) {
            this.__iterationPaths[curPath.join("")] = { path: curPath, price };
            return;
        }

        if (start === node && curPath.length > 3) {
            this.__iterationPaths[curPath.join("")] = { path: curPath, price };
            return;
        }

        for (let [index, neighbour] of Object.entries(this.__matrix[value])) {
            const name = this.__symbols[index];
            if (neighbour > 0)
                this.__findPaths(name, [...curPath], [...price, neighbour]);
        }
    }

    __formatPaths(isMarket = false) {
        return Object.values(this.__iterationPaths).filter(i => i.path[i.path.length - 1] == i.path[0]).map(i => {
            const path = [...i.path];
            i.path = [];
            for (let [index, node] of Object.entries(path)) {
                index = parseInt(index);
                const nextNode = path[index + 1];
                if (nextNode) {
                    const pair = this.__data.find(i =>
                        [node, nextNode].includes(i.baseAsset)
                        && [node, nextNode].includes(i.quoteAsset)
                    );
                    if (pair) {
                        let op = "buy";
                        if (nextNode === pair.quoteAsset && node === pair.baseAsset) op = "sell";
                        i.path = [
                            ...i.path,
                            {
                                orderType: isMarket ? OrderTypes.MARKET : OrderTypes.LIMIT,
                                symbol: pair.symbol,
                                price: i.price[index],
                                operation: op,
                                orderCount: pair.orderCount,
                            }
                        ];
                    }
                }
            }
            return {
                avgOrderCount: this.__avgOrderCount,
                pathIndex: i.path.reduce((a, b) => a + (b.orderCount > this.__avgOrderCount ? 1 : 0), 0),
                path: i.path
            };
        });
    }

    __printMatrix() {
        console.log(this.__symbols.map(i => i.padStart(15)).join("").padStart(this.__symbols.length * 15 + 11));
        for (let [n, i] of Object.entries(this.__matrix)) {
            console.log(this.__symbols[n].padEnd(10), i.map(i => i.toFixed(2).padStart(15)).join(""));
        }
    }

    __printCSVMatrix() {
        console.log(this.__symbols.map(i => JSON.stringify(i)).join(","));
        for (let [n, i] of Object.entries(this.__matrix)) {
            console.log([this.__symbols[n], ...i.map(i => JSON.stringify(i))].join(","));
        }
    }

    async __getResults(startPoints) {
        try {
            await this.__obtainBinanceInfo();

            this.__buildMatrix();
            const result = {};
            for (let point of startPoints) {
                try {
                    this.__iterationPaths = [];

                    this.__findPaths(point);

                    const paths = this.__formatPaths();
                    result[point] = paths;
                } catch (err) {
                    console.log(point, err.message);
                }
            }

            this.__buildMatrix(true);
            for (let point of startPoints) {
                try {
                    this.__iterationPaths = [];

                    this.__findPaths(point);

                    const paths = this.__formatPaths(true);
                    result[point] = [...result[point], ...paths];
                } catch (err) {
                    console.log(point, err.message);
                }
            }
            return result;
        } catch (err) {
            console.log(startPoints, err.message);
        }
    }

    async run(startPoints, maxDepth = 3) {
        if (!Array.isArray(startPoints)) throw new Error("Start points need to be an array!");
        this.__maxDepth = maxDepth;
        return this.__queue.enqueue(() => this.__getResults(startPoints));
    }
}

if (!isMainThread) {
    (async () => {
        const result = await BinanceTriangle.getInstance().run(workerData.startPoints, workerData.maxDepth);
        parentPort.postMessage(result);
    })();
}

export class BinanceWorker {
    static __instance;

    constructor() {
        this.__queue = new AutoQueue();
    }

    static getInstance() {
        if (!this.__instance) this.__instance = new BinanceWorker();
        return this.__instance;
    }

    async start(startPoints, maxDepth = 3) {
        return this.__queue.enqueue(() => {
            const worker = new Worker(__filename, { workerData: { startPoints, maxDepth } });
            return new Promise((resolve, reject) => {
                worker.on('error', reject);
                worker.on('message', resolve);
            })
        })
    }
}