import { FiatService } from "./FiatService";
import mastercardCurrencies from "../../static/mastercard_currencies.json";

export class MultiSystem extends FiatService {
    __tableHeader = [
        "Currency",
        ...this.__exchangeServicesFuncs.map(i => [`${i.name} Buy`, `${i.name} Sell`]).flat()
    ]

    static __instance;

    static getInstance() {
        if (!this.__instance) this.__instance = new MultiSystem();
        return this.__instance;
    }

    constructor() {
        super();
    }

    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];

        let allCurrencies = [...mastercardCurrencies];

        if (process.env.DEMO_REQUEST)
            allCurrencies = allCurrencies.slice(0, 1);

        for (let i of allCurrencies) {
            const currency = i;
            if (i === this.__baseCurrency) continue;
            if (process.env.DEMO_REQUEST)
                console.log(this.constructor.name, `${this.__baseCurrency} -> ${currency} -> ${this.__baseCurrency}`);
            const firstColumns = [];
            try {
                const buyRates = await this.__obtainAllExchangeRates({ sellAmount: this.__baseAmount, fromCurrency: currency, toCurrency: this.__baseCurrency });
                await new Promise(resolve => setTimeout(resolve, 2500));

                const sellRates = await this.__obtainAllExchangeRates({ buyAmount: this.__baseAmount, fromCurrency: currency, toCurrency: this.__baseCurrency });

                let result = [];

                for (let i = 0; i < this.__exchangeServicesFuncs.length; i++) {
                    const buy = buyRates[i];
                    const sell = sellRates[i];
                    const name = this.__exchangeServicesFuncs[i].name;

                    if (buy.name && buy.result && sell.name && sell.result) {
                        result.push({ name, buy: buy.result, sell: sell.result })
                    }
                    else result.push({ name, buy: null, sell: null });
                }

                lines.push([...firstColumns, currency, ...result.map(i => {
                    const { buy, sell } = i;
                    if (buy && sell) {
                        const { buy: { rate: buyRate }, sell: { rate: sellRate } } = i;
                        return [
                            buyRate,
                            sellRate
                        ];
                    } else return [null, null];
                }).flat()]);

                await new Promise(resolve => setTimeout(resolve, 2500));
            } catch (err) {
                console.log("Failed to obtain", this.constructor.name, "info for", currency, ":", err.message)
            }
        }
        return lines;
    }
}