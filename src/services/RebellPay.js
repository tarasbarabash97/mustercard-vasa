import Axios from "axios";
import Credentials from "../db/Credentials";
import { FiatService } from "./FiatService";

export class RebellPay extends FiatService {
    __tableHeader = [
        "Receiver",
        "To Send",
        null,
        "Rate",
        "To Receive",
        null,
        "Rate",
        "Result",
        null,
        "Profit (no fee), %"
    ]

    static __instance;
    __retriesNum = 0;

    __axios = Axios.create({
        baseURL: "https://api.rebellpay.com",
        headers: {
            "user-agent": "okhttp/4.8.0"
        },
        validateStatus: () => true
    });

    static getInstance() {
        if (!this.__instance) this.__instance = new RebellPay();
        return this.__instance;
    }

    constructor() {
        super();
        this.currencies = this.__axios("/info/currency/").catch(err => ({ data: { data } }));

        this.__axios.interceptors.request.use(async (config) => {
            const { accessToken } = await Credentials.findOne({ code: this.constructor.name });
            config.headers = {
                ...config.headers,
                "authorization": `Bearer ${accessToken}`
            }
            if (process.env.DEMO_REQUEST)
                console.log(JSON.stringify(config, null, "\t"));
            return config;
        })

        this.__axios.interceptors.response.use(async (response) => {
            if (process.env.DEMO_REQUEST)
                console.log(JSON.stringify(response.data, null, "\t"), response.status);

            if (response.status === 401 && this.__retriesNum < 1) {
                const { refreshToken } = await Credentials.findOne({ code: this.constructor.name });
                this.__retriesNum += 1;
                const { data: { data: { access, refresh } } } = await this.__axios.post("/users/token/refresh/", {
                    refresh: refreshToken
                });
                await Credentials.createOrUpdateOne({ code: this.constructor.name, refreshToken: refresh, accessToken: access });
            }
            else if (response.data.result = "success") {
                this.__retriesNum = 0;
                return response;
            };
            throw new Error("Invalid response with status", response.status, response.data);
        });
    }

    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];

        let { data: { data: { allCurrencies } } } = await this.currencies;
        allCurrencies = Object.keys(allCurrencies).sort((a, b) => a.localeCompare(b));

        if (process.env.DEMO_REQUEST)
            allCurrencies = allCurrencies.slice(0, 5);

        for (let i of allCurrencies) {
            const currency = i;

            if (i === this.__baseCurrency) continue;

            if (process.env.DEMO_REQUEST)
                console.log(this.constructor.name, `${this.__baseCurrency} -> ${currency} -> ${this.__baseCurrency}`);

            const firstColumns = [];

            try {
                const response = await this.__axios.post(`/info/commission-calculator-simple/`, {
                    amount: Math.round(this.__baseAmount * 100),
                    senderCurrency: this.__baseCurrency,
                    receiverCurrency: i
                });

                let { data: { data: { receivedAmountDecimal: amountToReceive, rate: { currencyRate } } } } = response;
                amountToReceive = parseFloat(amountToReceive);
                currencyRate = parseFloat(currencyRate);

                const result = await this.__obtainAllExchangeRates({ sellAmount: amountToReceive, fromCurrency: currency, toCurrency: this.__baseCurrency });
                lines.push(...result.map(i => {
                    const { name, result } = i;
                    if (result) {
                        const { result: { rate, amount } } = i;
                        if (isFinite(rate))
                            return [
                                ...firstColumns,
                                name,
                                this.__baseAmount,
                                this.__baseCurrency,
                                currencyRate,
                                amountToReceive,
                                currency,
                                rate,
                                amount,
                                this.__baseCurrency,
                                (amount - this.__baseAmount) / this.__baseAmount,
                            ];
                    }
                }));

                await new Promise(resolve => setTimeout(resolve, 2500));

            } catch (err) { console.log("Failed to obtain RebellPay info for", currency, ":", err.message) }
        }
        return lines;
    }
}