import { FiatService, ExchangeServices } from ".";
import Axios from "axios";

export class TransferGo extends FiatService {
    __axios = Axios.create({
        baseURL: "https://my.transfergo.com/api"
    });
    __tableHeader = [
        "Instant",
        "Receiver",
        "To Send",
        null,
        "Rate",
        "To Receive",
        null,
        "Rate",
        "Result",
        null,
        "Fee",
        null,
        "Profit (+fee), %",
        "Profit (no fee), %"
    ];

    static __instance;

    static getInstance() {
        if (!this.__instance) this.__instance = new TransferGo();
        return this.__instance;
    }

    constructor() {
        super();
        this.__classifiers = this.__axios("/classifiers/all").catch(err => ({ data: { corridors: {}, recipients } }));
    }

    __obtainInfo = async () => {
        let lines = [
            this.__tableHeader
        ];
        let { data: { corridors: { destinations, sources }, recipients } } = await this.__classifiers;

        if (process.env.DEMO_REQUEST)
            destinations = destinations.slice(0, 50);

        const testedCurrencies = [];

        const country = sources.find(i => i.currencies.includes(this.__baseCurrency));
        if (!country) return;

        for (let i of destinations) {
            for (let j of i.currencies) {
                const currency = j;

                if (testedCurrencies.includes(j) || j === this.__baseCurrency) {
                    if (process.env.DEMO_REQUEST)
                        console.log("Skipping", j, i.countryCode, ". Already got the result");

                    continue;
                };

                if (process.env.DEMO_REQUEST)
                    console.log(this.constructor.name, `${this.__baseCurrency} -> ${j} -> ${this.__baseCurrency}`);

                const recepientInfo = recipients.find(j => j.destinationCountry == i.countryCode);

                const { accountTypes } = recepientInfo;

                if (!accountTypes.map(i => i.type).includes("vgsCard")) {
                    if (process.env.DEMO_REQUEST)
                        console.log("Skipping", j, i.countryCode, ". Does not accept payment cards");

                    continue;
                };

                const firstColumns = [];

                const searchParams = new URLSearchParams({
                    calculationBase: "sendAmount",
                    amount: this.__baseAmount.toFixed(2),
                    fromCountryCode: country.countryCode,
                    toCountryCode: i.countryCode,
                    fromCurrencyCode: this.__baseCurrency,
                    toCurrencyCode: j
                })

                try {
                    let data = await this.__axios.get(`/transfers/quote?${searchParams.toString()}`);

                    let amountToReceive, currencyRate, fees;

                    try {
                        const { data: { deliveryOptions: { now: { paymentOptions: { card: { quote: { receivingAmount, rate, fees: { fees: fee } } } } } } } } = data;
                        amountToReceive = receivingAmount, currencyRate = rate, fees = fee;
                        firstColumns.push("true");
                    } catch (err) {
                        const { data: { deliveryOptions: { standard: { paymentOptions: { card: { quote: { receivingAmount, rate, fees: { fees: transferFee } } } } } } } } = data;
                        amountToReceive = receivingAmount, currencyRate = rate, fees = transferFee;
                        firstColumns.push("false");
                    }

                    const fee = Object.entries(fees).reduce((a, [key, value]) => {
                        if (/discount/gi.test(key)) return a;
                        return a + value.amount;
                    }, 0);

                    const { fields } = accountTypes.find(i => i.type === "vgsCard");

                    const { label } = fields.find(i => i.name === "cardNumber");

                    let exchangeRates = this.__exchangeServicesFuncs;

                    switch (label) {
                        case "recipient_form_label_visaandmastercardcardnumber":
                            break;
                        case "recipient_form_label_visacardnumber":
                            exchangeRates = exchangeRates.filter(i => i.name != ExchangeServices.MASTERCARD);
                            break;
                        case "recipient_form_label_mastercardcardnumber":
                            exchangeRates = exchangeRates.filter(i => ![ExchangeServices.VISA, ExchangeServices.WISE].includes(i.name));
                            break;
                    }

                    const result = await Promise.allSettled(
                        exchangeRates.map(
                            async i => ({
                                result: await this.__obtainExchangeRate(
                                    i.func,
                                    { sellAmount: amountToReceive, fromCurrency: currency, toCurrency: this.__baseCurrency }
                                ),
                                name: i.name
                            }))
                    );

                    lines.push(...result.map(i => {
                        const { name, result } = i.value;
                        if (result) {
                            const { result: { rate, amount } } = i.value;
                            if (isFinite(rate))
                                return [
                                    ...firstColumns,
                                    name,
                                    this.__baseAmount,
                                    this.__baseCurrency,
                                    currencyRate,
                                    amountToReceive,
                                    currency,
                                    rate,
                                    amount,
                                    this.__baseCurrency,
                                    fee,
                                    this.__baseCurrency,
                                    (amount - (this.__baseAmount + fee)) / this.__baseAmount,
                                    (amount - this.__baseAmount) / this.__baseAmount,
                                ];
                        }
                    }));

                    await new Promise(resolve => setTimeout(resolve, 2500));

                    testedCurrencies.push(j);
                } catch (err) { console.log("Failed to obtain", this.constructor.name, "info for", currency, ":", err.message) }
            }
        }
        return lines;
    }
}