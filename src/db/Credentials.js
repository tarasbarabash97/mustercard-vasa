import { model, Schema } from "mongoose";

const CredSchema = new Schema({
    code: String,
    refreshToken: String,
    accessToken: String
});

CredSchema.statics.createOrUpdateOne = async function (item) {
    let dbItem = await this.findOne({ code: item.code });
    if (dbItem) {
        if (item.refreshToken != dbItem.refreshToken)
            dbItem.refreshToken = item.refreshToken;
        if (item.accessToken != dbItem.accessToken)
            dbItem.accessToken = item.accessToken;
        await dbItem.save();
    }
    return dbItem;
}

export default model("Creds", CredSchema);