import { Router } from "express";
import { BinanceWorker } from "../services/binanceTriangular";
import { extendTimeoutMiddleware } from "../middleware/extendedTimeout";
import { getService } from "../services";
import axios from "axios";

const apiRouter = Router();

const webhookMiddleware = (req, res, next) => {
    const { body: { hookUrl } } = req;

    if (hookUrl) {
        try {
            new URL(hookUrl)
        } catch (err) {
            return res.status(400).json({ success: false, reason: "Invalid hookUrl", body: req.body });
        }
        return next();
    }

    return res.status(400).json({ success: false, reason: "No hookUrl provided", body: req.body });
}

async function sendResponse(result, service, hookUrl, cur) {
    if (process.env.DEMO_REQUEST) return console.log({ result, service, hookUrl });
    try {
        const response = await axios.post(hookUrl, JSON.stringify({ result, service }));
        console.log("Successfully posted a result for request", service, "-", cur, "and got response", response.data.success, response.data.reason);
    } catch (err) {
        console.log("Failed to post to:", hookUrl, "a result for request", service, "-", cur, err.message);
    }
}

apiRouter.post("/update", webhookMiddleware, (req, res) => {
    const { body: { service, currencies, hookUrl } } = req;
    const serv = getService(service);
    if (!serv) return res.status(404).json({ success: false, reason: "Service not found!", body: req.body });
    let currencyArray = [];
    if (Array.isArray(currencies) && currencies.every(i => i.length === 3)) currencyArray = currencies;
    if (currencyArray.length > 0) {
        for (let cur of currencyArray)
            serv.obtainInfo(cur).then((result) => sendResponse(result, service, hookUrl, cur));
    }
    else serv.obtainInfo().then((result) => sendResponse(result, service, hookUrl, currencies));
    return res.status(200).json({ success: true });
})

apiRouter.get("/binance", extendTimeoutMiddleware, async (req, res) => {
    let { query: { symbols, depth } } = req;
    depth = parseInt(depth || 3);
    res.setHeader("Content-Type", "application/json");
    if (isFinite(depth) && depth > 4) return res.json({ error: "Depth can't be over 4" });
    if (symbols == undefined) return res.json({ error: "Invalid symbols" });
    symbols = JSON.parse(symbols);

    try {
        const result = await BinanceWorker.getInstance().start(symbols, depth);
        res.end(JSON.stringify(result));
    } catch (err) {
        res.end(JSON.stringify({ error: err.message }));
    }
})

apiRouter.post("/binance", webhookMiddleware, (req, res) => {
    let { body: { hookUrl, symbols, depth } } = req;
    depth = parseInt(depth || 3);
    if (isFinite(depth) && depth > 4)
        return res.json({ success: false, reason: "Depth can't be over 4", body: req.body });

    if (symbols == undefined || !Array.isArray(symbols))
        return res.json({ success: false, error: "Invalid symbols", body: req.body });

    BinanceWorker.getInstance().start(symbols, depth).then((result) => sendResponse(result, "Binance", hookUrl));
    return res.status(200).json({ success: true });
});

export default apiRouter;