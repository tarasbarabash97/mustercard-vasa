import express, { Router } from "express";
import apiRouter from "./api";
import path from "path";

const router = Router();
router.use("/api", apiRouter);

export const initRoutes = app => {
    app.use(router);

    app.use("/static", express.static(path.join(__dirname, '../../static')));

    app.use((err, req, res, next) => {
        console.log(err);
        res.status(502).json({
            error: err.message
        });
    })
}