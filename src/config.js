import mongoose from "mongoose";

export const startApp = (app) => {
    const port = process.env.PORT || 3000;
    const server = app.listen(port, () => console.log(`App is running on ${port}`));
    server.setTimeout(10 * 60 * 1000);
}

export async function initDB() {
    return mongoose.connect(process.env.DB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log(`A connection to the db is established!`);
    }).catch(err => {
        console.log(err);
    });
}