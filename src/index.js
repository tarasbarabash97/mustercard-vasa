import "dotenv/config";
import express from "express";
import { initDB, startApp } from "./config";
import { initRoutes } from "./routes";
import compression from "compression";
import bodyParser from "body-parser";

const app = express();
app.use(compression({ level: 9 }));
app.use(bodyParser.json());

initDB();

initRoutes(app);
startApp(app);